var gulp       = require('gulp');
var less       = require('gulp-less');
var rename     = require('gulp-rename');
var runseq     = require('run-sequence');
var minifycss  = require('gulp-minify-css');

var dirs = {
    less: './less',
};

// ####  ##### #####   #   #   # #     #####
// #   # #     #      # #  #   # #       #
// #   # ####  ####  #   # #   # #       #
// #   # #     #     ##### #   # #       #
// #   # #     #     #   # #   # #       #
// ####  ##### #     #   #  ###  #####   #

gulp.task('default', function (cb) {
    runseq(
        ['less', 'watch-less'],
    cb);
});

gulp.task('less', function (cb) {
    runseq(
        ['less'],
    cb);
});

gulp.task('watch-less', function (cb) {
    runseq(
        ['watch-less'],
    cb);
});

gulp.task('less', function (cb) {
    gulp.src(dirs.less+'/**/*.less')
        .pipe(less())
        .pipe(minifycss())
        .pipe(rename("min.css"))
        .on('error', function(err) {
            console.error(err);
        })
        .pipe(gulp.dest(dirs.less));

    cb();
});

gulp.task('watch-less', function() {  
    gulp.watch('./less/**/*.less' , ['less']);
});
