$(document).ready(function() {
    $('section').height($(window).height());
    window.slickOn = false;
    $('.carousel').on('unslick', function() {
        slickOn = false;
    });

    function getCarousel() {
        var width = $(window).width();
        var options = {
            slidesToScroll: 1,
            slidesToShow: 3,
            infinite: true,
            arrows: false,
            responsive: [
                {
                  breakpoint: 766,
                  settings: 'unslick'
                },
                {
                  breakpoint: 576,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true
                  }
                }
            ]
        };
        if (width > 576 && width < 768) {
            if (!slickOn) {
                options = {
                    slidesToScroll: 1,
                    slidesToShow: 1,
                    infinite: true,
                    arrows: false,
                    rows: 3,
                    responsive: [
                        {
                          breakpoint: 1000,
                          settings: 'unslick'
                        },
                        {
                          breakpoint: 769,
                          settings: 'unslick'
                        },
                        {
                          breakpoint: 768,
                          settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                            infinite: true,
                            arrows: false,
                            rows: 3,
                          }
                        },
                        {
                          breakpoint: 576,
                          settings: 'unslick'
                        }
                    ]
                };
            }
        }
        if (!slickOn) {
            slickOn = true;
            $('.carousel').slick(options);
        }
    }
    getCarousel();

    $(window).on('resize', function() {
        $('section').height($(window).height());
        getCarousel();
    });

    $('a.goTo').on('click', function() {
        var index = $(this).attr('href').replace(/^#/,'');

        $('html, body').animate({
            scrollTop: $('section').eq(index).offset().top
        }, 700);

        return false;
    });
    
    $('.sliderNext').on('click', function() {
        $('.carousel').slick('slickNext');
    });

    $('.sliderPrev').on('click', function() {
        $('.carousel').slick('slickPrev');
    })
});
